class Ship (private val size: Int) {
    var cells = arrayListOf<Cell>()
    var status: String = SHIP_STATUS_ALIVE

    private var dx = 0
    private var dy = 0

    fun make(cell: Cell, direction: String): Boolean {

        checkDirection(direction)
        addCells(cell)

        val isOnBoard = isOnBoard()
        if (!isOnBoard){
            cells.clear()
        }
        return isOnBoard
    }

    private fun checkDirection(direction: String){
        when (direction) {
            SHIP_DIRECTION_NORTH -> dy = +1
            SHIP_DIRECTION_EAST -> dx = +1
            SHIP_DIRECTION_WEST -> dx = -1
            SHIP_DIRECTION_SOUTH -> dy = -1
        }
    }

    private fun addCells(cell: Cell){
        cells.add(cell)
        for (index in 2..size) {
            cells.add(Cell(cells.last().x + dx, cells.last().y + dy))
        }
    }

    private fun isOnBoard(): Boolean {
        for (shipCell in cells){
            if (!shipCell.isOnBoard())
                return false
        }
        return true
    }


    fun fire(cell:Cell):String {
        for (shipCell in cells) {
            if (shipCell.equalsTo(cell) && shipCell.status != CELL_STATUS_FIRED){
                shipCell.status = CELL_STATUS_FIRED
                cell.status = CELL_STATUS_FIRED
            }
        }
        return cell.status
    }

    fun isDeadOrAlive(){    // is dead or not
        if (size == cells.count { it.status == CELL_STATUS_FIRED })
            status = SHIP_STATUS_DEAD
    }
}