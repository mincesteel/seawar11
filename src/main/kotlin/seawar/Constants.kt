const val CELL_STATUS_EMPTY = "Empty"
const val CELL_STATUS_FIRED = "Fired"
const val CELL_STATUS_MISSED = "Missed"


const val BOARD_X_MIN = 1
const val BOARD_X_MAX = 10
const val BOARD_Y_MAX = 10
const val BOARD_Y_MIN = 1


const val SHIP_STATUS_ALIVE = "Alive"
const val SHIP_STATUS_DEAD = "Dead"

const val SHIP_DIRECTION_NORTH = "n"
const val SHIP_DIRECTION_EAST = "e"
const val SHIP_DIRECTION_WEST = "w"
const val SHIP_DIRECTION_SOUTH = "s"

const val GAME_MAX_NUMBER_OF_SHIPS = 10

const val GAME_STATUS_ONGOING = "Ongoing"
const val GAME_STATUS_LOST = "Lost"
const val GAME_STATUS_WIN = "Win"

const val GAME_WIN_MESSAGE = "Congratulations!!! You won the game!!"
const val GAME_LOSE_MESSAGE = "You lost <o_o>. How sad."

const val GAME_ONGOING_ERROR_MESSAGE = "Oops! Game is still ongoing."
const val GAME_SHIP_NOT_ADDED = "WRONG COORDINATES FOR SHIPS. NOT ADDED."

/*
4
1 1 e
3
3 1 e
3
5 1 e
2
7 1 e
2
9 1 e
2
9 4 e
1
1 7 e
1
3 7 e
1
5 7 e
1
7 7 e


 */