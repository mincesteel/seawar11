import seawar.*
class Cell(var x: Int, var y: Int) {
    var status = CELL_STATUS_EMPTY
    private val MIN_Y_NEIHBOURHOOD = y - 1
    private val MIN_X_NEIHBOURHOOD = x - 1
    private val MAX_Y_NEIHBOURHOOD = y + 1
    private val MAX_X_NEIHBOURHOOD = x + 1

    fun isOnBoard() = (x <= BOARD_X_MAX)
                            && (x >= BOARD_X_MIN)
                            && (y <= BOARD_Y_MAX)
                            && (y >= BOARD_Y_MIN)

    fun equalsTo(other: Cell) = (x == other.x) && (y == other.y)

    fun isNear(other: Cell) = (other.x <= MAX_X_NEIHBOURHOOD)
            && (other.x >= MIN_X_NEIHBOURHOOD)
            && (other.y <= MAX_Y_NEIHBOURHOOD)
            && (other.y >= MIN_Y_NEIHBOURHOOD)
}