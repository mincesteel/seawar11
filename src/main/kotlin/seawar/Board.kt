//import com.sun.org.apache.xpath.internal.operations.Bool

class Board {
    var ships = arrayListOf<Ship>()
    var missedCells = arrayListOf<Cell>()
    var board = arrayOf<IntArray>()
    fun initializeBoard() {
        for (i in BOARD_X_MIN..BOARD_X_MAX + 1) {
            var array = IntArray(11);
            for (j in BOARD_Y_MIN..BOARD_Y_MAX + 1) {
                array += 0;
            }
            board += array
        }
    }

    fun addShip(ship: Ship) {
        if (checkShipSpace(ship)) {
            ships.add(ship)
            for (sourceCell in ship.cells) {
                board[sourceCell.x][sourceCell.y] = 1
            }
        }
    }

    private fun checkShipSpace(ship: Ship): Boolean {
        for (shipOnBoard in ships) {
            if (checkShipCollision(ship, shipOnBoard)) {
                return false
            }
        }
        return true
    }

    private fun checkShipCollision(shipOne: Ship, shipTwo: Ship): Boolean {
        for (shipOneCell in shipOne.cells)
            for (shipTwoCell in shipTwo.cells) {
                if (shipOneCell.isNear(shipTwoCell))
                    return true
            }
        return false
    }

    fun fire(cell: Cell): Boolean {
        if (canFire(cell)) {
            for (ship in ships) {
                if (ship.fire(cell) == CELL_STATUS_FIRED) {
                    ship.isDeadOrAlive()
                    break;
                }
            }
            if (cell.status != CELL_STATUS_FIRED) {
                cell.status = CELL_STATUS_MISSED
                missedCells.add(cell)
            }
        }
        return cell.status == CELL_STATUS_FIRED
    }

    private fun canFire(cell: Cell): Boolean {
        for (missedCell in missedCells)
            if (missedCell.equalsTo(cell))
                return false
        return true
    }
}