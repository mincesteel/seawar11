import java.io.File
import kotlin.io.*

class Game {

    val myBoard = Board()
    val enemyBoard = Board()
    var status: String = GAME_STATUS_ONGOING
    val filename = "resources/ShipPlacement.txt"
    val enemyFilename = "resources/enemyShipPlacement.txt"
    var HitOrMiss: Boolean = false
//    var strlist = listOf<String>()
//    var enemyStrlist = listOf<String>()

    private fun readFileToList(fileName: String): List<String> = File(fileName).readLines()
    private val strlist = readFileToList(filename).toMutableList()
    private val enemystrlist = readFileToList(enemyFilename).toMutableList()

    fun start() {
        myBoard.initializeBoard()
        enemyBoard.initializeBoard()

        println("FIRST LEARN THE RULES")

        println("Now placing ships for player number one\n")
        while (myBoard.ships.count() != GAME_MAX_NUMBER_OF_SHIPS) {
            placeShipOnBoard(myBoard, strlist)
        }
        println("Now placing ships for player number two\n")
        while (enemyBoard.ships.count() != GAME_MAX_NUMBER_OF_SHIPS) {
            placeShipOnBoard(enemyBoard, enemystrlist)
        }
        println("Placement phase ended")

        while (status == GAME_STATUS_ONGOING) {
            println("player one's turn")
            myTurn()
            print(HitOrMiss)
            while (HitOrMiss == true) {
                myTurn()
                checkGameStatus()
                if(status == GAME_STATUS_LOST) break
                printBoards(enemyBoard)
            }
            if(status == GAME_STATUS_LOST) break
            HitOrMiss = false
            println("end of player one turn")
            println("player two's turn")
            enemyTurn()
            while (HitOrMiss == true) {
                enemyTurn()
                checkGameStatus()
                if(status == GAME_STATUS_LOST)break
                printBoards(myBoard)
            }
            if(status == GAME_STATUS_LOST) break
            println("end of player two turn")
        }

        endGameMessage()
    }

    fun printBoards(board: Board) {
        println("\n")
        for (i in 1..10) {
            for (j in 1..10) {
                print(board.board[j][i])
            }
            println()
        }
    }

    private fun placeShipOnBoard(holdingBoard: Board, strlist: MutableList<String>) {
        println("\nPlease enter ship size ")
        val ship = Ship(strlist.get(0).toInt())
        println(strlist.get(0).toInt())
        strlist.removeAt(0)
//        val ship = Ship(readLine()!!.toInt())
//        val ship = Ship(inputStream.bufferedReader().readLine().toInt())
        println("Please enter ship head coordinates and its direction ")
//        val (x: String, y: String, direction: String) = readLine()!!.split(" ")
//        val(x:String, y:String,direction:String) = inputStream.bufferedReader().readLine()!!.split(" ")
        val (x: String, y: String, direction: String) = strlist[0].split(" ")
        val ix = x.toInt();
        val iy = y.toInt();
        print(" x = $x y= $y direction = $direction")
        strlist.removeAt(0)

        ship.make(Cell(ix, iy), direction)
        val check = holdingBoard.ships.count()
        holdingBoard.addShip(ship)
//        println(check)
//        println(holdingBoard.count)
//        println(holdingBoard.ships.count())
        if (holdingBoard.ships.count() == check)
            println(GAME_SHIP_NOT_ADDED)
    }


    private fun myTurn() {
        println("First player fires")

        printBoards(myBoard)
        printBoards(enemyBoard)

        val (x: String, y: String) = readLine()!!.split(" ")
        HitOrMiss = enemyBoard.fire(Cell(x.toInt(), y.toInt()))
        if (HitOrMiss) {
            enemyBoard.board[x.toInt()][y.toInt()] = 2
            println("INJURED OR KILLED")
        } else {
            enemyBoard.board[x.toInt()][y.toInt()] = 3
            println("MISSED")
        }
        //Удар по клетке которую выбрал враг
        //Выбрать клетку по которой атакуем мы
    }

    private fun enemyTurn() {
        print("Second player fires\n")
        val (x: String, y: String) = readLine()!!.split(" ")
        HitOrMiss = myBoard.fire(Cell(x.toInt(), y.toInt()))
        if (HitOrMiss) {
            myBoard.board[x.toInt()][y.toInt()] = 2
            println("INJURED OR KILLED")
        } else {
            myBoard.board[x.toInt()][y.toInt()] = 3
            println("MISSED")
        }

        //Получаем ответочку от врага
        //Не сдох ли он уже
        //Если нет то какую клетку атакует
        //Какую клетку он атакует
        //Отправляем врагу клетку которую атакуем
    }

    private fun checkGameStatus() {
        if (myBoard.ships.count { it.status == SHIP_STATUS_DEAD } == 0)
            status = GAME_STATUS_LOST
        if(enemyBoard.ships.count { it.status == SHIP_STATUS_DEAD } == 0)
            status = GAME_STATUS_LOST
    }

    private fun endGameMessage() {
        when (status) {
            GAME_STATUS_WIN -> println(GAME_WIN_MESSAGE)
            GAME_STATUS_LOST -> println(GAME_LOSE_MESSAGE)
            GAME_STATUS_ONGOING -> println(GAME_ONGOING_ERROR_MESSAGE)
        }
    }

}